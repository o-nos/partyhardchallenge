package com.spacesoul.partyhardchallenge;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.parse.Parse;
import com.parse.ParseObject;
import com.spacesoul.partyhardchallenge.model.Comment;


/**
 * Created by alexandr on 01.07.15.
 */
public class App extends Application {

    // Debugging switch
    public static final boolean APPDEBUG = true;

    // Debugging tag for the application
    public static final String APPTAG = "ASDF places";

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "K02MurzKah11BKewWV7qCJ2gtQXbGWuJZmPfNU6x", "rl1nCqFW3egfmzo8FimuJZWim04Bjny4H7Z7vOyY");
        ParseObject.registerSubclass(Comment.class);
    }

}
