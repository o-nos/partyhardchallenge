package com.spacesoul.partyhardchallenge.authorization;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;

import com.spacesoul.partyhardchallenge.places.PrePickActivity;

/**
 * Created by alexandr on 04.07.15.
 */
public class BaseFragment extends Fragment {

    protected void replaceFragment(Fragment fragment, int containerId, boolean addToBackStack) {
        FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
        ft.replace(containerId, fragment);

//        ft.setTransition()

        if (addToBackStack) {
            ft.addToBackStack(fragment.getClass().getName());
        }

        ft.commit();
    }

    protected void openActionActivity(){
        Intent actionIntent = new Intent(getActivity(), PrePickActivity.class);
        actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(actionIntent);
    }

}
