package com.spacesoul.partyhardchallenge.authorization;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.spacesoul.partyhardchallenge.App;
import com.spacesoul.partyhardchallenge.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexandr on 02.07.15.
 */
public class LogInUserFragment extends BaseFragment {

    @Bind(R.id.username_edit_text)
    EditText usernameEditText;
    @Bind(R.id.password_edit_text)
    EditText passwordEditText;
    @Bind(R.id.reg_button)
    Button regButton;

    @Bind(R.id.username_edit_text_layout)
    TextInputLayout usernameInputLayout;
    @Bind(R.id.password_edit_text_layout)
    TextInputLayout passwordInputLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.log_in_user_fragment, container, false);

        ButterKnife.bind(this, rootView);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });

        return rootView;
    }

    private void logIn() {

        usernameInputLayout.setErrorEnabled(false);
        passwordInputLayout.setErrorEnabled(false);

        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        boolean validationError = false;

        if (username.length() == 0) {
            validationError = true;
            usernameInputLayout.setError(getString(R.string.error_blank_username));
        }
        if (password.length() == 0) {
            validationError = true;
            passwordInputLayout.setError(getString(R.string.error_blank_password));
        }

        if (validationError)
            return;

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.progress_login));
        dialog.show();

        // logs in user here
        ParseUser.logInInBackground(username, password, new LogInCallback() {

            public void done(ParseUser user, ParseException e) {

                dialog.dismiss();
                if (user != null) {
                    // Hooray! The user is logged in.
                    Toast.makeText(getActivity(),
                            R.string.the_message,
                            Toast.LENGTH_SHORT)
                            .show();

                    openActionActivity();

                } else {
                    // Signup failed. Look at the ParseException to see what happened.
                    Toast.makeText(getActivity(),
                            getString(R.string.login_error),
                            Toast.LENGTH_LONG).show();
                    if (App.APPDEBUG)
                        Log.d(App.APPTAG, Integer.toString(e.getCode()));
                }
            }
        });
    }
}
