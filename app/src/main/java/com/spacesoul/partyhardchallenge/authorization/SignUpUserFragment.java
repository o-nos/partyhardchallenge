package com.spacesoul.partyhardchallenge.authorization;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.spacesoul.partyhardchallenge.App;
import com.spacesoul.partyhardchallenge.R;

import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexandr on 01.07.15.
 */
public class SignUpUserFragment extends BaseFragment {

    private static final String PATTERN = ".+\\@.+\\.[a-z]{2,4}";
    @Bind(R.id.username_edit_text)
    EditText usernameEditText;
    @Bind(R.id.password_edit_text)
    EditText passwordEditText;
    @Bind(R.id.password_again_edit_text)
    EditText passwordAgainEditText;
    @Bind(R.id.reg_button)
    Button regButton;
    @Bind(R.id.email_edit_text)
    EditText emailEditText;

    @Bind(R.id.username_edit_text_layout)
    TextInputLayout usernameInputLayout;
    @Bind(R.id.password_edit_text_layout)
    TextInputLayout passInputLayout;
    @Bind(R.id.password_again_edit_text_layout)
    TextInputLayout passAgainInputLayout;
    @Bind(R.id.email_edit_text_layout)
    TextInputLayout emailInputLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sing_up_user_fragment, container, false);

        ButterKnife.bind(this, rootView);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        return rootView;
    }

    private void signUp() {

        usernameInputLayout.setErrorEnabled(false);
        passInputLayout.setErrorEnabled(false);
        passAgainInputLayout.setErrorEnabled(false);
        emailInputLayout.setErrorEnabled(false);

        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String passAgain = passwordAgainEditText.getText().toString().trim();
        String email = emailEditText.getText().toString().trim();

        boolean validationError = false;

        if (username.length() == 0) {
            validationError = true;
            usernameInputLayout.setError(getString(R.string.error_blank_username));
        }
        if (password.length() == 0) {
            validationError = true;
            passInputLayout.setError(getString(R.string.error_blank_password));
        }
        if (!password.equals(passAgain)) {
            validationError = true;
            passAgainInputLayout.setError(getString(R.string.error_mismatched_passwords));
        }
        if (!Pattern.matches(PATTERN, email)) {
            validationError = true;
            emailInputLayout.setError(getString(R.string.wrong_email));
        }

        // If there is a validation error, display the error and return
        if (validationError) {
            return;
        }

        // setting processDialog to show the user a long term sign up operation
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.progress_signup));
        dialog.show();

        // sign up process here
        ParseUser newUser = new ParseUser();
        newUser.setUsername(username);
        newUser.setPassword(password);
        newUser.setEmail(email); // is optional

        // other fields can be set just like with ParseObject
//                    user.put("phone", "650-253-0000");

        // create and automatically log in new user
        newUser.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {

                dialog.dismiss();
                if (e == null) {
                    // Hooray! Let them use the app now.
                    Toast.makeText(getActivity(),
                            "Have nice time here :)", Toast.LENGTH_SHORT)
                            .show();

                    openActionActivity();

                } else {
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    Toast.makeText(getActivity(),
                            e.getMessage(),
                            Toast.LENGTH_LONG).show();
                    if (App.APPDEBUG)
                        Log.d(App.APPTAG, Integer.toString(e.getCode()));
                }
            }
        });

    }

}
