package com.spacesoul.partyhardchallenge.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.parse.ParseUser;
import com.spacesoul.partyhardchallenge.R;
import com.spacesoul.partyhardchallenge.places.PrePickActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // if we have already logged in, we pass right to the PrePickActivity
        if (ParseUser.getCurrentUser() == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.main_holder, new HelloUserFragment())
                    .commit();
        } else {
            startActivity(new Intent(MainActivity.this, PrePickActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
