package com.spacesoul.partyhardchallenge.authorization;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.spacesoul.partyhardchallenge.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexandr on 02.07.15.
 */
public class HelloUserFragment extends BaseFragment implements View.OnClickListener {

    @Bind(R.id.signUp_button)
    Button signUpButton;
    @Bind(R.id.logIn_button)
    Button logInButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.set_user_fragment, container, false);

        ButterKnife.bind(this, rootView);

        signUpButton.setOnClickListener(this);

        logInButton.setOnClickListener(this);

        return rootView;

    }

    /* якщо батони виконують схожу дію, краще імплементувати View.OnClickListener
     * і зробити дію через switch
     */
    @Override
    public void onClick(View v) {

        Fragment fragment = null;
        switch(v.getId()){
            case R.id.signUp_button :
                fragment = new SignUpUserFragment();
                break;
            case R.id.logIn_button :
                fragment = new LogInUserFragment();
                break;
        }

        replaceFragment(fragment, R.id.main_holder, true);

    }
}
