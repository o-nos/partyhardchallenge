package com.spacesoul.partyhardchallenge.places;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.spacesoul.partyhardchallenge.R;
import com.spacesoul.partyhardchallenge.model.Comment;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by onos on 10/13/2015.
 */
public class HeaderRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String DATA_FORMAT = "HH:mm dd/MM/yyyy";
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private PostCommentCallback commentCallback;
    private List<Comment> commentList;
    private Context context;
    private String address;
    private String level;
    private String number;

    public HeaderRecyclerViewAdapter(Context context, List<Comment> commentList, String address, String number, String level) {
        this.address = address;
        this.context = context;
        this.number = number;
        this.level = level;
        this.commentList = commentList;
        try {
            this.commentCallback = (PlaceExtendedActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            View item = LayoutInflater.from(context).inflate(R.layout.item_comment, parent, false);
            return new VHItem(item);
        } else if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            View header = LayoutInflater.from(context).inflate(R.layout.header_place_info, parent, false);
            return new VHHeader(header);
        }

        throw new RuntimeException("there is no type that matches the type " +
                viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            //cast holder to VHItem and set data
            VHItem item = (VHItem) holder;
            item.userName.setText(getItem(position).getUserName());
            SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT, Locale.getDefault());
            item.date.setText(format.format(getItem(position).getCreatedAt()));
            item.text.setText(getItem(position).getText());
        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
            VHHeader header = (VHHeader) holder;
            header.placePriceLvlTextView.setText(String.format(context.getString(R.string.price_lvl), level));
            header.addressTextView.setText(address);
            if (!number.equals("")) {
                header.placePhoneNumberTextView.setText(number);
            } else {
                header.placePhoneNumberTextView.setText(context.getString(R.string.no_number));
            }
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private Comment getItem(int position) {
        return commentList.get(position - 1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView userName;
        TextView date;
        TextView text;

        public VHItem(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            date = (TextView) itemView.findViewById(R.id.comment_date);
            text = (TextView) itemView.findViewById(R.id.comment_text);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView placePriceLvlTextView;
        TextView addressTextView;
        TextView placePhoneNumberTextView;
        EditText commentEditText;
        Button leaveComment;

        public VHHeader(View itemView) {
            super(itemView);
            placePriceLvlTextView = (TextView) itemView.findViewById(R.id.place_price_level);
            addressTextView = (TextView) itemView.findViewById(R.id.place_address_textview);
            placePhoneNumberTextView = (TextView) itemView.findViewById(R.id.place_phone_number);
            leaveComment = (Button) itemView.findViewById(R.id.comment_button);
            commentEditText = (EditText) itemView.findViewById(R.id.comment_edit_text);
            leaveComment.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            // callback to fragment to post comment and reload extended view
            commentCallback.postComment(commentEditText.getText().toString().trim());
        }
    }
}