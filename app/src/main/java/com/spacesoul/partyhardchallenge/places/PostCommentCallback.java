package com.spacesoul.partyhardchallenge.places;

/**
 * Created by onos on 10/19/2015.
 */
public interface PostCommentCallback {

    void postComment(String text);

}
