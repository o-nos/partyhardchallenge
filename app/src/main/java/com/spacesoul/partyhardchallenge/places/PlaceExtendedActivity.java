package com.spacesoul.partyhardchallenge.places;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.spacesoul.partyhardchallenge.App;
import com.spacesoul.partyhardchallenge.R;
import com.spacesoul.partyhardchallenge.model.Comment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by onos on 10/9/2015.
 */
public class PlaceExtendedActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        PostCommentCallback {

    @Bind(android.R.id.progress)
    ProgressBar progress;
    @Bind(R.id.simpleRecyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.place_imageview)
    ImageView imageView;
    @Bind(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    private List<Comment> comments;
    private String address;
    private String level;
    private String number;
    private Place currentPlace;
    private String placeID;
    private GoogleApiClient googleApiClient;
    private final Handler handler = new Handler();
    private static final String CREATED_AT = "createdAt";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_extended_view);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        placeID = getIntent().getStringExtra(PrePickActivity.PICKED_PLACE_ID);

        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, placeID);
        placeResult.setResultCallback(UpdatePlaceDetailsCallback);

    }

    private ResultCallback<PlaceBuffer> UpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(App.APPTAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }

            // Get the Place object from the buffer.
            currentPlace = places.get(0);
            if (App.APPDEBUG) Log.e(App.APPTAG, "Place is " + currentPlace.getName());

            // fill the fields to use this info in header
            address = currentPlace.getAddress().toString();
            level = priceDescription(currentPlace.getPriceLevel());
            if (!currentPlace.getPhoneNumber().toString().equals("")) {
                number = currentPlace.getPhoneNumber().toString();
            } else {
                number = getString(R.string.no_number);
            }

            // NOW I CAN FILL OUT TEXTVIEWs AND LOAD COMMENTS

            collapsingToolbarLayout.setTitle(currentPlace.getName());
            recyclerView.setHasFixedSize(false); // 'cause we will expand view with new comments

            // Use a linear layout manager
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(PlaceExtendedActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);

            // LOAD ALL STUFF
            new Thread(new LoadCommentsPhotoTask(true, imageView.getWidth(), imageView.getHeight(),
                    false, ""))
                    .start();

            places.release();
        }
    };

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(App.APPTAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                getString(R.string.connection_failed) + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    private String priceDescription(int priceLevel) {
        switch (priceLevel) {
            case 0:
                return getString(R.string.free);
            case 1:
                return getString(R.string.inexpensive);
            case 2:
                return getString(R.string.moderate);
            case 3:
                return getString(R.string.expensive);
            case 4:
                return getString(R.string.very);
            default:
                return getString(R.string.moderate);
        }
    }

    @Override
    public void postComment(String text) {
        if (!text.equals("")) {

            new Thread(new LoadCommentsPhotoTask(false, 0, 0, // 'cause we do not load photo
                    true, text)) // this time we upload and reload only comments
                    .start();

        }
    }

    private class LoadCommentsPhotoTask implements Runnable {

        private static final String FIND_ERROR = "Could not find comments for the place";
        private static final String PLACE_ID_FIELD = "place_id";
        private static final String COMMENT_TABLE = "Comment";
        private boolean loadPhoto;
        private boolean newComment;
        private int mHeight;
        private int mWidth;
        private String commentText;
        private AttributedPhoto attributedPhoto = null; // do not sure it's needed

        LoadCommentsPhotoTask(boolean loadPhoto, int mHeight, int mWidth, boolean newComment, String commentText) {
            this.mHeight = mHeight;
            this.mWidth = mWidth;
            this.loadPhoto = loadPhoto;
            this.newComment = newComment;
            this.commentText = commentText;
        }

        public void run() {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            });

            // LOAD PHOTO ON BACKGROUND
            if (loadPhoto) {
                PlacePhotoMetadataResult result = Places.GeoDataApi
                        .getPlacePhotos(googleApiClient, placeID).await();

                if (result.getStatus().isSuccess()) {
                    PlacePhotoMetadataBuffer photoMetadataBuffer = result.getPhotoMetadata();
                    if (photoMetadataBuffer.getCount() > 0) {
                        // Get the first bitmap and its attributions.
                        PlacePhotoMetadata photo = photoMetadataBuffer.get(0);
                        CharSequence attribution = photo.getAttributions();
                        // Load a scaled bitmap for this photo.
                        Bitmap image = photo.getScaledPhoto(googleApiClient, mWidth, mHeight).await()
                                .getBitmap();

                        attributedPhoto = new AttributedPhoto(attribution, image);
                    }
                    // Release the PlacePhotoMetadataBuffer.
                    photoMetadataBuffer.release();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (attributedPhoto != null)
                            imageView.setImageBitmap(attributedPhoto.bitmap);
                    }
                });
            }

            if (newComment) {
                // upload new comments to Parse
                Comment comment = new Comment();
                comment.setText(commentText);
                comment.setPlaceID(currentPlace.getId());
                comment.setUserName(ParseUser.getCurrentUser().getUsername());
                try {
                    comment.save();
                } catch (ParseException e) {
                    Toast.makeText(PlaceExtendedActivity.this, e.getCode(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            // FETCH COMMENTS ON BACKGROUND
            ParseQuery<Comment> query = ParseQuery.getQuery(COMMENT_TABLE);
            query.whereEqualTo(PLACE_ID_FIELD, placeID);
            query.orderByAscending(CREATED_AT);
            try {
                comments = new ArrayList<>(query.find());
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d(App.APPTAG, FIND_ERROR);

            }

            handler.post(new Runnable() {
                @Override
                public void run() {

                    /** Мені здається, можна перезагрузити адаптер так:
                     * recyclerView.getAdapter().notifyDataSetChanged();
                     * але чи підійде тут цей варіант?
                     */

                    HeaderRecyclerViewAdapter headerRecyclerViewAdapter =
                            new HeaderRecyclerViewAdapter(PlaceExtendedActivity.this, comments,
                                    address, number, level);

                    recyclerView.setAdapter(headerRecyclerViewAdapter);
                }
            });

            handler.post(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            });
        }

        class AttributedPhoto {
            public final CharSequence attribution; // some info
            public final Bitmap bitmap;

            public AttributedPhoto(CharSequence attribution, Bitmap bitmap) {
                this.attribution = attribution;
                this.bitmap = bitmap;
            }
        }
    }
}