package com.spacesoul.partyhardchallenge.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by alexandr on 04.08.15.
 */
@ParseClassName("Comment")
public class Comment extends ParseObject {

    private static final String USER_NAME_FIELD = "user_name";
    private static final String PLACE_ID_FIELD = "place_id";
    private static final String TEXT_FIELD = "text";

    public void setUserName(String id) {
        put(USER_NAME_FIELD, id);
    }

    public String getUserName() {
        return getString(USER_NAME_FIELD);
    }

    public void setPlaceID(String id) {
        put(PLACE_ID_FIELD, id);
    }

    public String getPlaceID() {
        return getString(PLACE_ID_FIELD);
    }

    public void setText(String title) {
        put(TEXT_FIELD, title);
    }

    public String getText() {
        return getString(TEXT_FIELD);
    }


}
